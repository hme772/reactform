import useInput from "../hooks/useInput";

const isNotEmpty = (value) => value.trim() !== "";
const isEmail = (value) => value.includes("@");

const BasicForm = (props) => {
  const {
    value: firstInput,
    isValid: firstInputIsValid,
    hasError: firstInputHasError,
    valueChangedHandler: firstInputChangedHandler,
    inputBlurHandler: firstInputBlurHandler,
    reset: firstInputReset,
  } = useInput(isNotEmpty);
  const {
    value: lastInput,
    isValid: lastInputIsValid,
    hasError: lastInputHasError,
    valueChangedHandler: lastInputChangedHandler,
    inputBlurHandler: lastInputBlurHandler,
    reset: lastInputReset,
  } = useInput(isNotEmpty);

  const {
    value: emailInput,
    isValid: emailInputIsValid,
    hasError: emailInputHasError,
    valueChangedHandler: emailInputChangedHandler,
    inputBlurHandler: emailInputBlurHandler,
    reset: emailInputReset,
  } = useInput(isEmail);

  let formIsValid = false;
  if (firstInputIsValid && lastInputIsValid && emailInputIsValid) {
    formIsValid = true;
  }

  const formSubmissionHandler = (event) => {
    event.preventDefault();

    if (!formIsValid) {
      return;
    }
    console.log("submitted", firstInput, lastInput, emailInput);
    firstInputReset();
    lastInputReset();
    emailInputReset();
  };

  const firstInputClasses = firstInputHasError
    ? "form-control invalid"
    : "form-control";
  const lastInputClasses = lastInputHasError
    ? "form-control invalid"
    : "form-control";
  const emailInputClasses = emailInputHasError
    ? "form-control invalid"
    : "form-control";

  return (
    <form onSubmit={formSubmissionHandler}>
      <div className="control-group">
        <div className={firstInputClasses}>
          <label htmlFor="first">First Name</label>
          <input
            type="text"
            id="first"
            value={firstInput}
            onChange={firstInputChangedHandler}
            onBlur={firstInputBlurHandler}
          />
          {firstInputHasError && (
            <p className="error-text">Please enter first name</p>
          )}
        </div>
        <div className={lastInputClasses}>
          <label htmlFor="last">Last Name</label>
          <input
            type="text"
            id="last"
            value={lastInput}
            onChange={lastInputChangedHandler}
            onBlur={lastInputBlurHandler}
          />
          {lastInputHasError && (
            <p className="error-text">Please enter last name</p>
          )}
        </div>
      </div>
      <div className={emailInputClasses}>
        <label htmlFor="email">E-Mail Address</label>
        <input
          type="text"
          id="email"
          value={emailInput}
          onChange={emailInputChangedHandler}
          onBlur={emailInputBlurHandler}
        />
        {emailInputHasError && <p className="error-text">Please enter email</p>}
      </div>
      <div className="form-actions">
        <button disabled={!formIsValid}>Submit</button>
      </div>
    </form>
  );
};

export default BasicForm;
