import { useState } from "react";

const useInput = (validateValue) => {
  const [enteredValue, setEnteredValue] = useState("");
  const [isTouched, setIsTouched] = useState(false);
  const valueIsValid = validateValue(enteredValue);
  const hasError = !valueIsValid && isTouched;

  const valueChangedHandler = (event) => {
    //for every keystrok
    setEnteredValue(event.target.value);
  };
  const inputBlurHandler = (event) => {
    //for every lost focus
    setIsTouched(true);
  };

  const reset = () => {
    setEnteredValue('');
    setIsTouched(false);
  };
  return {
    value: enteredValue,
    isValid:valueIsValid,
    hasError,
    valueChangedHandler,
    inputBlurHandler,
    reset
  };
};
export default useInput;
